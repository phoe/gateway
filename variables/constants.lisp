;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; GATEWAY
;;;; © Michał "phoe" Herda 2017
;;;; classes/standard-date.lisp

(in-package :gateway/variables)

(defvar *date-granularity-units*
  '(:year :month :day :hour :minute :second :nanosecond))
